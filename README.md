## Bienvenido
Este es el README del backend de peanutsment, a continuacion se encuentran las instrucciones para configurar el proyecto para poder trabajar con el
## Configuracion para Linux

Es necesario crear un entorno virtual para comodidad, en windows ejecuta los siguientes comandos
1. python3 -m venv env
2. source env/bin/activate (**Este comando es necesario ejecutarlo siempre antes de trabajar en el proyecto**)
3. python3 -m pip install -r requirements.txt

---

## Configuracion para Windows

Es necesario crear un entorno virtual para comodidad, en windows ejecuta los siguientes comandos
1. py -m venv env
2. .\env\Scripts\activate (**Este comando es necesario ejecutarlo siempre antes de trabajar en el proyecto**)
3. py -m pip install -r requirements.txt

---

## Configuracion de la base de datos

Una vez realizado esto, necesitas instalar mysql y crear la base de datos asi como el usuario que utilizara django, para esto ejecuta los siguientes comandos en mysql
1. CREATE DATABASE peanutsment;
2. CREATE USER 'djangouser'@'%' IDENTIFIED BY 'peanutsment';
3. GRANT ALL ON peanutsment.* TO 'djangouser'@'%';



---

## Flujo de trabajo de Django

Con Django no necesitas ejecutar practicamente comandos de sql una vez hecha la configuracion inicial, en el archivo PEANUTSMENT_API/models.py se encuentran las clases que representan las tablas de la base de datos.
Cada que realices un cambio, para que se efectue en la base y **despues de activar** el entorno virtual, ejecuta:

python manage.py makemigrations PEANUTSMENT_API  

Ese comando creara el script que modificara la base de datos, despues, para ejecutarlo:

python manage.py migrate

Con ello se mostraran mensajes de que se efectuaron los cambios

---
##Ejecutar el proyecto
Para ejecutar el proyecto **despues de activar** el entorno virtual ejecuta:

python manage.py runserver

## Documentacion importante
QuerySet(para hacer consultas) https://docs.djangoproject.com/en/3.2/ref/models/querysets/

Fields(para crear modelos) https://docs.djangoproject.com/en/3.2/ref/models/fields/


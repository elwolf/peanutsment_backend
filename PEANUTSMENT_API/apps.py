from django.apps import AppConfig


class PeanutsmentApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PEANUTSMENT_API'

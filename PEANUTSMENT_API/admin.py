from django.contrib import admin

# Register your models here.
from PEANUTSMENT_API.models import Hotel, Paquete, EstadoHabitacion, IngresosHotel, HabitacionHotel, UsuarioMetodoPago, \
    UsuarioPaquete, IngresosParqueDiversiones, Restaurante, IngresosRestaurante

admin.site.register(Hotel)
admin.site.register(Paquete)
admin.site.register(EstadoHabitacion)
admin.site.register(IngresosHotel)
admin.site.register(HabitacionHotel)
admin.site.register(UsuarioMetodoPago)
admin.site.register(UsuarioPaquete)
admin.site.register(IngresosParqueDiversiones)
admin.site.register(Restaurante)
admin.site.register(IngresosRestaurante)

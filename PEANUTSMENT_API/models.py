from django.contrib.auth.models import User
from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from django.core.mail import send_mail
from datetime import date
from creditcards.models import CardNumberField, CardExpiryField, SecurityCodeField

# Create your models here.
from django_rest_passwordreset.signals import reset_password_token_created


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    email_plaintext_message = "{}?token={}".format(reverse('password_reset:reset-password-request'),
                                                   reset_password_token.key)

    print(send_mail(
        # title:
        "Restablecer contraseña de {title}".format(title="MuseumMax"),
        # message:
        email_plaintext_message,
        # from:
        "mailasd@asd.com",
        # to:
        [reset_password_token.user.email]
    ))

class Hotel(models.Model):
    id_hotel = models.IntegerField(primary_key=True)
    habitaciones = models.IntegerField(null=False)


class EstadoHabitacion(models.Model):
    id_estado_habitacion = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=50)


class IngresosHotel(models.Model):
    id_ingresos_hotel = models.AutoField(primary_key=True)
    habitaciones_ocupadas = models.IntegerField(null=False)
    ingresos_externos = models.DecimalField(max_digits=16, decimal_places=2, null=False)
    ingresos_sistema = models.DecimalField(max_digits=16, decimal_places=2, null=False)
    fecha = models.DateTimeField(null=False)
    hotel = models.ForeignKey(Hotel, null=False, on_delete=models.CASCADE)


class HabitacionHotel(models.Model):
    id_habitacion_hotel = models.IntegerField(primary_key=True)
    numero = models.IntegerField(null=False)
    estado = models.ForeignKey(EstadoHabitacion, null=False, on_delete=models.CASCADE)
    hotel = models.ForeignKey(Hotel, null=False, on_delete=models.CASCADE)
    fecha_inicio_ocupacion = models.DateTimeField(null=False)
    fecha_fin_ocupacion = models.DateTimeField(null=False)


class UsuarioMetodoPago(models.Model):
    usuario = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    #numero_tarjeta = models.CharField(max_length=17, null=False)
    numero_tarjeta = CardNumberField(('card number'))
    #cvv = models.CharField(max_length=3, null=False)
    cvv = SecurityCodeField(('security code'))
    
    nombreEnTarjeta = models.CharField(max_length=50, null=False)
    #mesVencimiento = models.DateField(null=False)
    #anioVencimiento = models.DateField(null=False)
    vencimiento = CardExpiryField(('expiration date'),default=date(2030,1,31))
    numTelefono = models.CharField(max_length=10, null=False)
    calleyNum = models.CharField(max_length=50, null=False)
    estado = models.CharField(max_length=50, null=False)
    ciudad = models.CharField(max_length=50, null=False)
    colonia = models.CharField(max_length=50, null=False)
    codigoPostal = models.CharField(max_length=5, null=False)


class Paquete(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    precio = models.DecimalField(null=False, decimal_places=2, max_digits=18)
    costo_hotel = models.DecimalField(null=True, decimal_places=2, max_digits=18)
    costo_restaurante = models.DecimalField(null=True, decimal_places=2, max_digits=18)
    costo_parque = models.DecimalField(null=True, decimal_places=2, max_digits=18)
    description = models.CharField(max_length=2000)
    url_imagen = models.CharField(max_length=2000)
    fecha_inicio_vigencia = models.DateField()
    fecha_fin_vigencia = models.DateField()
    duracion = models.IntegerField(null=False)


class UsuarioPaquete(models.Model):
    usuario = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    usuario_metodo_pago = models.ForeignKey(UsuarioMetodoPago, null=False, on_delete=models.CASCADE)
    paquete = models.ForeignKey(Paquete, null=False, on_delete=models.CASCADE)
    fecha_compra = models.DateTimeField(null=False)
    fecha_inicio = models.DateTimeField(null=False)
    fecha_fin = models.DateTimeField(null=False)
    cancelado = models.BooleanField(null=False, default=False)


class IngresosParqueDiversiones(models.Model):
    id_ingresos_parque_diversiones = models.AutoField(primary_key=True)
    ingresos_externos = models.DecimalField(max_digits=16, decimal_places=2, null=False)
    ingresos_sistema = models.DecimalField(max_digits=16, decimal_places=2, null=False)
    fecha = models.DateTimeField(null=False)


class Restaurante(models.Model):
    id_restaurante = models.IntegerField(primary_key=True)
    mesas = models.IntegerField(null=False)


class IngresosRestaurante(models.Model):
    id_ingresos_restaurante = models.AutoField(primary_key=True)
    mesas_ocupadas = models.IntegerField(null=False)
    ingresos_sistema = models.DecimalField(max_digits=16, decimal_places=2, null=False)
    ingresos_externos = models.DecimalField(max_digits=16, decimal_places=2, null=False)
    fecha = models.DateTimeField(null=False)
    restaurante = models.ForeignKey(Restaurante, null=False, on_delete=models.CASCADE)

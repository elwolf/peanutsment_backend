from datetime import datetime

from django.contrib.auth.models import User


class CompraDePaquetes:
    id_paquete: int
    fecha_inicio: datetime
    fecha_fin: datetime
    id_usuario_Metodo_pago: int
    usuario: User

    def __init__(self, id_paquete: int, fecha_inicio: datetime, fecha_fin: datetime, id_usuario_Metodo_pago: int,
                 usuario: User):
        if id_paquete:
            self.id_paquete = id_paquete
        if fecha_inicio:
            self.fecha_inicio = fecha_inicio
        if fecha_fin:
            self.fecha_fin = fecha_fin
        if usuario:
            self.usuario = usuario
        if id_usuario_Metodo_pago:
            self.idUsuarioMetodoPago = id_usuario_Metodo_pago


class ReagendacionDeCompra:
    id_compra: int
    fecha_inicio: datetime
    fecha_fin: datetime

    def __init__(self, id_compra: int, fecha_inicio: datetime, fecha_fin: datetime):
        if id_compra:
            self.id_compra = id_compra
        if fecha_inicio:
            self.fecha_inicio = fecha_inicio
        if fecha_fin:
            self.fecha_fin = fecha_fin

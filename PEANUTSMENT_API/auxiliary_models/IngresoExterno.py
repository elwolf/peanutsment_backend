from datetime import datetime

from django.db.models.fields import DecimalField


class RegistroExH:
    id_ingresos_hotel: int
    ingresos_externos: DecimalField
    fecha: datetime

    def __init__(self, id_ingresos_hotel: int, ingresos_externos: DecimalField, fecha: datetime):
        if id_ingresos_hotel:
            self.id_ingresos_hotel = id_ingresos_hotel
        if ingresos_externos:
            self.ingresos_externos = ingresos_externos
        if fecha:
            self.fecha = fecha

class RegistroExR:
    id_ingresos_restaurante: int
    ingresos_externos: DecimalField
    fecha: datetime

    def __init__(self, id_ingresos_restaurante: int, ingresos_externos: DecimalField, fecha: datetime):
        if id_ingresos_restaurante:
            self.id_ingresos_hotel = id_ingresos_restaurante
        if ingresos_externos:
            self.ingresos_externos = ingresos_externos
        if fecha:
            self.fecha = fecha

class RegistroExP:
    id_ingresos_parque_diversiones: int
    ingresos_externos: DecimalField
    fecha: datetime

    def __init__(self, id_ingresos_parque_diversiones: int, ingresos_externos: DecimalField, fecha: datetime):
        if id_ingresos_parque_diversiones:
            self.id_ingresos_parque_diversiones = id_ingresos_parque_diversiones
        if ingresos_externos:
            self.ingresos_externos = ingresos_externos
        if fecha:
            self.fecha = fecha


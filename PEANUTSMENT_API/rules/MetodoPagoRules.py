from django.contrib.auth.models import User
from django.db.models.fields import CharField#, DateField
from rest_framework import status
from creditcards.models import CardExpiryField, CardNumberField, SecurityCodeField

from PEANUTSMENT_API.models import UsuarioMetodoPago


def eliminar_metodo_pago(id_metodo: int) -> int:
    """Returns the status code of the request if the data was saved successfully"""
    try:
        metodo: UsuarioMetodoPago = UsuarioMetodoPago.objects.get(id=id_metodo)
        metodo.delete()
        metodo.save()
        return status.HTTP_200_OK
    except Exception as e:
        return status.HTTP_500_INTERNAL_SERVER_ERROR

# CON django-credit-cards
def agregar_metodo_pago(usuario: User, numero_tarjeta: CardNumberField, cvv: SecurityCodeField, nombreEnTarjeta: CharField,
                        vencimiento:CardExpiryField,
                        numTelefono: CharField,
                        calleyNum: CharField, estado: CharField, ciudad: CharField, colonia: CharField,
                        codigoPostal: CharField) -> int:
    try:
        metodo = UsuarioMetodoPago.objects.create(
            usuario=usuario,
            numero_tarjeta=numero_tarjeta,
            cvv=cvv,
            nombreEnTarjeta=nombreEnTarjeta,
            vencimiento = vencimiento,
            numTelefono=numTelefono,
            calleyNum=calleyNum,
            estado=estado,
            ciudad=ciudad,
            colonia=colonia,
            codigoPostal=codigoPostal
        )
        metodo.save()
        return status.HTTP_200_OK
    except Exception as e:
        return status.HTTP_500_INTERNAL_SERVER_ERROR

#CON django-credit-cards
def editar_metodo_pago(usuario: User, idUsuarioMetodoPago: int,  numero_tarjeta: CardNumberField, cvv: SecurityCodeField,
                       nombreEnTarjeta: CharField,
                       vencimiento:CardExpiryField,
                       numTelefono: CharField,
                       calleyNum: CharField, estado: CharField, ciudad: CharField, colonia: CharField,
                       codigoPostal: CharField) -> int:
    try:
        m_pago: UsuarioMetodoPago = UsuarioMetodoPago.objects.get(id=idUsuarioMetodoPago, usuario=usuario)

        m_pago.numero_tarjeta = numero_tarjeta
        m_pago.cvv = cvv
        m_pago.nombreEnTarjeta = nombreEnTarjeta
        m_pago.vencimiento = vencimiento
        m_pago.numTelefono = numTelefono
        m_pago.calleyNum = calleyNum
        m_pago.estado = estado
        m_pago.ciudad = ciudad
        m_pago.colonia = colonia
        m_pago.codigoPostal = codigoPostal

        m_pago.save()
        return status.HTTP_200_OK
    except Exception as e:
        return status.HTTP_500_INTERNAL_SERVER_ERROR
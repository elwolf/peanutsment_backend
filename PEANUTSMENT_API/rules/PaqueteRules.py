from django.utils import timezone
from rest_framework import status

from PEANUTSMENT_API.auxiliary_models.paquetes import CompraDePaquetes, ReagendacionDeCompra
from PEANUTSMENT_API.models import UsuarioPaquete


def compra_paquetes(compra_data: CompraDePaquetes) -> int:
    """Returns the status code of the request if the data was saved successfully"""
    try:
        nuevo_paquete = UsuarioPaquete(
            usuario=compra_data.usuario,
            usuario_metodo_pago_id=compra_data.idUsuarioMetodoPago,
            paquete_id=compra_data.id_paquete,
            fecha_compra=timezone.now(),
            fecha_inicio=compra_data.fecha_inicio,
            fecha_fin=compra_data.fecha_fin
        )
        nuevo_paquete.save()
        return status.HTTP_200_OK
    except Exception as e:
        return status.HTTP_500_INTERNAL_SERVER_ERROR


def cancelacion_de_paquete(id_compra: int) -> int:
    """Returns the status code of the request if the data was saved successfully"""
    try:
        compra: UsuarioPaquete = UsuarioPaquete.objects.get(id=id_compra)
        compra.cancelado = True
        compra.save()
        return status.HTTP_200_OK
    except Exception as e:
        return status.HTTP_500_INTERNAL_SERVER_ERROR


def reagendacion_de_paquete(reagendacion_data: ReagendacionDeCompra) -> int:
    """Returns the status code of the request if the data was saved successfully"""
    try:
        compra: UsuarioPaquete = UsuarioPaquete.objects.get(id=reagendacion_data.id_compra)
        compra.fecha_fin = reagendacion_data.fecha_fin
        compra.fecha_inicio = reagendacion_data.fecha_inicio
        compra.save()
        return status.HTTP_200_OK
    except Exception as e:
        return status.HTTP_500_INTERNAL_SERVER_ERROR

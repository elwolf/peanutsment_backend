from django.contrib.auth.models import User
from django.db.models.fields import DateField, FloatField
from rest_framework import status

from PEANUTSMENT_API.models import IngresosHotel,IngresosParqueDiversiones,IngresosRestaurante


def registrar_ingresos_externos(idRegistro:int,ingresos_externos)->int:

    try:

        if idRegistro == 1:
            registro: IngresosHotel = IngresosHotel.objects.create(
                ingresos_sistema=0,
                ingresos_externos=ingresos_externos
            )
            registro.save()

        elif idRegistro == 2:
            registro: IngresosRestaurante = IngresosRestaurante.objects.create(
                ingresos_sistema=0,
                ingresos_externos=ingresos_externos
            )
            registro.save()

        elif idRegistro == 3:
            registro: IngresosParqueDiversiones = IngresosParqueDiversiones.objects.create(
                ingresos_sistema=0,
                ingresos_externos=ingresos_externos
            )
            registro.save()

        else:
            return status.HTTP_406_NOT_ACCEPTABLE


    except Exception as e:
        return status.HTTP_500_INTERNAL_SERVER_ERROR
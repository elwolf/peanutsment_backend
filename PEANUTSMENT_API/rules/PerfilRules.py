from django.contrib.auth.models import User
from django.db.models.fields import CharField, EmailField
from rest_framework import status

def edicion_perfil(usuario: User,nombre_usuario:CharField,nombres:CharField,apellidos:CharField,correo:EmailField) -> int:
    """Returns the status code of the request if the data was saved successfully"""
    try:
        us: User = User.objects.get(id=usuario.id)
        us.username = nombre_usuario
        us.first_name = nombres
        us.last_name = apellidos
        us.email = correo
        us.save()
        return status.HTTP_200_OK
    except Exception as e:
        return status.HTTP_500_INTERNAL_SERVER_ERROR
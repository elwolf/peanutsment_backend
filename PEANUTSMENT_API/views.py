# Create your views here.
from django import http
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.db.models import Sum
from django.db.models import Q
from django.db.models.aggregates import Aggregate
from rest_framework import status, generics
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from PEANUTSMENT_API.auxiliary_models.IngresoExterno import RegistroExH, RegistroExP, RegistroExR
from PEANUTSMENT_API.auxiliary_models.paquetes import CompraDePaquetes, ReagendacionDeCompra
from PEANUTSMENT_API.models import EstadoHabitacion, HabitacionHotel, Hotel, IngresosHotel, IngresosParqueDiversiones, IngresosRestaurante, Restaurante, UsuarioMetodoPago, UsuarioPaquete, Paquete
from PEANUTSMENT_API.rules.IngresosRules import registrar_ingresos_externos
from PEANUTSMENT_API.rules.MetodoPagoRules import agregar_metodo_pago, eliminar_metodo_pago , editar_metodo_pago
from PEANUTSMENT_API.rules.PaqueteRules import compra_paquetes, cancelacion_de_paquete, reagendacion_de_paquete
from PEANUTSMENT_API.rules.PerfilRules import edicion_perfil
from PEANUTSMENT_API.serializers import HotelSerializer, EstadoHabitacionSerializer, IngresosHotelSerializer, \
    HabitacionHotelSerializer, IngresosParqueDiversionesSerializer, IngresosRestauranteSerializer, PaqueteSerializer, \
    RestauranteSerializer, ChangePasswordSerializer, \
    UsuarioMetodoPagoSerializer, UsuarioPaqueteSerializer, UsuarioSerializer


class HotelView(APIView):

    def get(self, request):
        hoteles = Hotel.objects.all().related("duenio").prefetch("habitaciones")

        hoteles_serializados = HotelSerializer(hoteles, many=True)
        return Response(hoteles_serializados.data)


class EstadoHabitacionView(APIView):

    def get(self, request):
        estado_habitacion = EstadoHabitacionSerializer(EstadoHabitacionSerializer.objects.all(), many=True)
        return Response(estado_habitacion.data)


class IngresosHotelView(APIView):

    def get(self, request):
        ingresos_hotel = IngresosHotelSerializer(IngresosHotelSerializer.objects.all(), many=True)
        return Response(ingresos_hotel.data)


class HabitacionHotelView(APIView):

    def get(self, request):
        habitacion_hotel = HabitacionHotelSerializer(HabitacionHotelSerializer.objects.all(), many=True)
        return Response(habitacion_hotel.data)


class IngresosParqueDiversionesView(APIView):

    def get(self, request):
        ingresos_parque_diversiones = IngresosParqueDiversionesSerializer(
            IngresosParqueDiversionesSerializer.objects.all(), many=True)
        return Response(ingresos_parque_diversiones.data)


class RestauranteView(APIView):

    def get(self, request):
        restaurantes = Restaurante.objects.all()
        restaurantes_serializados = RestauranteSerializer(restaurantes, many=True)
        return Response(restaurantes_serializados.data)


class IngresosRestauranteView(APIView):

    def get(self, request):
        ingresos_parque_diversiones = IngresosParqueDiversionesSerializer(
            IngresosParqueDiversionesSerializer.objects.all(), many=True)
        return Response(ingresos_parque_diversiones.data)


class Login(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        """ CU1: Iniciar sesion """
        required_fields = ["correo", "contraseña"]
        not_provided = [required_field for required_field in required_fields if required_field not in request.data]
        if not_provided:
            message = "Faltan los campos: " + ", ".join(not_provided)
            status_code = status.HTTP_206_PARTIAL_CONTENT
            return Response(message, status_code)
        user: User = User.objects.get_by_natural_key(request.data["correo"])
        if not user.check_password(request.data["contraseña"]):
            return Response("contraseña invalida", status=status.HTTP_400_BAD_REQUEST)
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })


class Logout(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        """ CU1.1: Iniciar sesion """
        request.user.auth_token.delete()
        logout(request)
        return Response(status=status.HTTP_200_OK)


class UserCRUD(APIView):

    def post(self, request):
        """ CU2: Registrar usuario """
        response: str = ""
        response_obj: dict = None
        status_code: int = status.HTTP_200_OK
        try:
            user_request_data = request.data
            try:
                existing_user = User.objects.get(
                    Q(username=user_request_data["correo"]) | Q(email=user_request_data["correo"]))
                if existing_user:
                    response = "El usuario ya existe"
                    status_code = status.HTTP_208_ALREADY_REPORTED
            except User.DoesNotExist:
                pass
            if status_code == status.HTTP_200_OK:
                new_user:User = User.objects.create_user(user_request_data["correo"], user_request_data["correo"],
                                                    user_request_data["contraseña"])
                new_user.first_name = user_request_data["nombre"]
                new_user.save()
                token = Token.objects.create(user=new_user)
                response_obj = {
                    'token': token.key,
                    'user_id': new_user.pk,
                    'email': new_user.email
                }

        except Exception as e:
            response = "Ocurrio un error"
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return Response(response_obj if response_obj else response, status=status_code)


class ChangePasswordView(generics.UpdateAPIView):
    """ CU3: Recuperar contraseña """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = [IsAuthenticated]

    def get_object(self, querset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {'message': 'Contraseña cambiada exitosamente'}

            return Response(response, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CompraDePaquetesView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        required_fields = ["idPaquete", "fechaInicio", "fechaFin", "idUsuarioMetodoPago"]
        message = "Tu compra se completo de forma exitosa"
        status_code: int
        not_provided = [required_field for required_field in required_fields if required_field not in request.data]
        if not_provided:
            message = "Faltan los campos: " + ", ".join(not_provided)
            status_code = status.HTTP_206_PARTIAL_CONTENT
        else:
            helper = CompraDePaquetes(
                id_paquete=request.data["idPaquete"],
                fecha_inicio=request.data["fechaInicio"],
                fecha_fin=request.data["fechaFin"],
                id_usuario_Metodo_pago=request.data["idUsuarioMetodoPago"],
                usuario=request.user)
            status_code = compra_paquetes(helper)
            message = "Ocurrio un error" if status_code != status.HTTP_200_OK else message
        return Response(message, status=status_code)

    def delete(self, request, pk):
        """
        CU6:Cancelar paquete de entretenimiento
        """
        if not pk:
            return Response("Proporciona el id de la compra", status=status.HTTP_206_PARTIAL_CONTENT)
        status_code = cancelacion_de_paquete(pk)
        return Response("Compra cancelada" if status_code == status.HTTP_200_OK else "Error", status=status_code)

    def update(self, request):
        """
        CU7:Reagendar paquete de entretenimiento
        """
        required_fields = ["idCompra", "fechaInicio", "fechaFin"]
        message = "Se reagendo el periodo de tu paquete."
        status_code: int
        not_provided = [required_field for required_field in required_fields if required_field not in request.data]
        if not_provided:
            message = "Faltan los campos: " + ", ".join(not_provided)
            status_code = status.HTTP_206_PARTIAL_CONTENT
        else:
            status_code = reagendacion_de_paquete(ReagendacionDeCompra(
                id_compra=request.data["idCompra"],
                fecha_inicio=request.data["fechaInicio"],
                fecha_fin=request.data["fechaFin"]
            ))
            message = message if status_code == status.HTTP_200_OK else "error"
        return Response(message, status=status_code)



class HistorialCompras(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """parametros GET"""
        hist = UsuarioPaquete.objects.filter(usuario=request.user)
        hist_compras = UsuarioPaqueteSerializer(hist, many=True)
        return Response(hist_compras.data, status=status.HTTP_200_OK)

    def post(self, request):
        """parametros Post"""
        return Response(status=status.HTTP_200_OK)


class DetallesCompra(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """parametros GET"""
        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        """parametros Post"""
        required_fields = ["idUsuarioPaquete"]
        status_code: int
        not_provided = [required_field for required_field in required_fields if required_field not in request.data]
        if not_provided:
            message = "Faltan los campos: " + ", ".join(not_provided)
            status_code = status.HTTP_206_PARTIAL_CONTENT
            return Response(message, status=status_code)
        else:
            detalle_compra = UsuarioPaquete.objects.filter(usuario=request.user,
                                                    id=request.data["idUsuarioPaquete"])
            com = UsuarioPaqueteSerializer(detalle_compra, many=True)
            return Response(com.data, status=status.HTTP_200_OK)


class VerPerfil(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """parametros GET"""
        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        usuario = User.objects.filter(id=request.user.id)
        return Response(UsuarioSerializer(usuario, many=True).data, status=status.HTTP_200_OK)


class EditarPerfil(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """parametros GET"""
        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        """parametros Post"""
        required_fields = ["nombre_usuario","nombres","apellidos", "correo"]
        message = "Cambios en el perfil realizados"
        status_code: int
        not_provided = [required_field for required_field in required_fields if required_field not in request.data]
        if not_provided:
            message = "Faltan los campos: " + ", ".join(not_provided)
            status_code = status.HTTP_206_PARTIAL_CONTENT
        else:
            status_code = edicion_perfil(
                request.user,
                nombre_usuario=request.data["nombre_usuario"],
                nombres=request.data["nombres"],
                apellidos=request.data["apellidos"],
                correo=request.data["correo"]
            )
            message = message if status_code == status.HTTP_200_OK else "error"
        return Response(message, status=status_code)


class GestionarMetodoPago(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """parametros GET"""
        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        metodos = UsuarioMetodoPago.objects.filter(usuario=request.user)
        return Response(UsuarioMetodoPagoSerializer(metodos, many=True).data, status=status.HTTP_200_OK)


class AgregarMetodoPago(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """parametros GET"""
        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        """parametros Post"""
        required_fields = ["numero_tarjeta", "cvv","nombreEnTarjeta","vencimiento",#"mesVencimiento", "anioVencimiento",
                        "numTelefono","calleyNum","estado","ciudad","colonia","codigoPostal"]
        message = "Metodo de pago agregado"
        status_code: int
        not_provided = [required_field for required_field in required_fields if required_field not in request.data]
        if not_provided:
            message = "Faltan los campos: " + ", ".join(not_provided)
            status_code = status.HTTP_206_PARTIAL_CONTENT
        else:
            status_code = agregar_metodo_pago(
                request.user,
                numero_tarjeta=request.data["numero_tarjeta"],
                nombreEnTarjeta=request.data["nombreEnTarjeta"],
                cvv=request.data["cvv"],
                #mesVencimiento=request.data["mesVencimiento"],
                #anioVencimiento=request.data["anioVencimiento"],
                vencimiento = request.data["vencimiento"],
                numTelefono=request.data["numTelefono"],
                calleyNum=request.data["calleyNum"],
                estado=request.data["estado"],
                ciudad=request.data["ciudad"],
                colonia=request.data["colonia"],
                codigoPostal=request.data["codigoPostal"]
            )
            message = message if status_code == status.HTTP_200_OK else "error"
        return Response(message, status=status_code)


class EditarMetodoPago(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """parametros GET"""
        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        """parametros Post"""
        required_fields = ["idUsuarioMetodoPago","nombreEnTarjeta","numero_tarjeta", "cvv","vencimiento",#"mesVencimiento", "anioVencimiento",
                        "numTelefono","calleyNum","estado","ciudad","colonia","codigoPostal"]
        message = "Metodo de pago editado correctamente"
        status_code: int
        not_provided = [required_field for required_field in required_fields if required_field not in request.data]
        if not_provided:
            message = "Faltan los campos: " + ", ".join(not_provided)
            status_code = status.HTTP_206_PARTIAL_CONTENT
        else:
            status_code = editar_metodo_pago(
                request.user,
                idUsuarioMetodoPago = request.data["idUsuarioMetodoPago"],
                nombreEnTarjeta=request.data["nombreEnTarjeta"],
                numero_tarjeta=request.data["numero_tarjeta"],
                cvv=request.data["cvv"],
                #mesVencimiento=request.data["mesVencimiento"],
                #anioVencimiento=request.data["anioVencimiento"],
                vencimiento=request.data["vencimiento"],
                numTelefono=request.data["numTelefono"],
                calleyNum=request.data["calleyNum"],
                estado=request.data["estado"],
                ciudad=request.data["ciudad"],
                colonia=request.data["colonia"],
                codigoPostal=request.data["codigoPostal"]
            )
            message = message if status_code == status.HTTP_200_OK else "error"
        return Response(message, status=status_code)


class eliminarMetodo(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """CU15: Eliminar metodo de pago"""
        """parametros GET"""
        return Response(status=status.HTTP_200_OK)

    def delete(self, request):
        status_code: int
        """parametros DELETE"""
        required_fields = ["idUsuarioMetodoPago"]
        message = "Metodo de pago eliminado correctamente"
        not_provided = [required_field for required_field in required_fields if required_field not in request.data]
        if not_provided:
            message = "Faltan los campos: " + ", ".join(not_provided)
            status_code = status.HTTP_206_PARTIAL_CONTENT
        else:
            status_code = eliminar_metodo_pago(request.data["idUsuarioMetodoPago"])
            message = message if status_code == status.HTTP_200_OK else "error"
        return Response(message, status=status_code)


class verPaquetes(APIView):
    def get(self, request):
        """CU16: Ver Paquete"""
        """parametros GET"""

        return Response(PaqueteSerializer(Paquete.objects.all(), many=True).data, status=status.HTTP_200_OK)

    def post(self, request):
        """parametros Post"""
        paquetes = PaqueteSerializer(Paquete.objects.get(id=request.data["id"]))
        return Response(paquetes.data,status=status.HTTP_200_OK)


class estadisticasFlujo(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """CU17: Ver estadisticas del flujo economico"""
        """parametros GET"""
        InRes = IngresosRestaurante.objects.aggregate(sumaRS=Sum('ingresos_sistema'))

        InResX = IngresosRestaurante.objects.aggregate(sumaRX=Sum('ingresos_externos'))
        InResT: float = float(InRes['sumaRS']) + float(InResX['sumaRX'])

        InHot = IngresosHotel.objects.aggregate(sumaHS=Sum('ingresos_sistema'))
        InHotX = IngresosHotel.objects.aggregate(sumaHX=Sum('ingresos_externos'))

        InHotT: float = float(InHot['sumaHS']) + float(InHotX['sumaHX'])

        InPaD = IngresosParqueDiversiones.objects.aggregate(sumaPDS=Sum('ingresos_sistema'))
        InPaDX = IngresosParqueDiversiones.objects.aggregate(sumaPDX=Sum('ingresos_externos'))

        InPaDT: float = float(InPaD['sumaPDS']) + float(InPaDX['sumaPDX'])

        total:float = InResT+InHotT+InPaDT
        message='{"ingresos_restaurante":', InResT,'"ingresos_hotel":', InHotT,'"ingresos_parque":', InPaDT, '"Total:"',total,'}'
        return Response(message, status=status.HTTP_200_OK)

class ocupacion(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """CU18: Ver ocupacion en porcentajes"""
        """parametros GET"""
        ocupacion_hotel = IngresosHotel.objects.aggregate(sumaO=Sum('habitaciones_ocupadas'))
        habit = Hotel.objects.aggregate(sumaH=Sum('habitaciones'))

        perOH:int = (int(ocupacion_hotel["sumaO"])*100)/(int(habit["sumaH"]))
        
        ocupacion_restaurante=IngresosRestaurante.objects.aggregate(suma=Sum('mesas_ocupadas'))
        mesas = Restaurante.objects.aggregate(sumaR=Sum('mesas'))

        perOR:int = (int(ocupacion_restaurante["suma"])*100)/(int(mesas["sumaR"]))

        message='{ "hotel": { "cantidadOcupado":',perOH,'"cantidadDisponible":',(100-perOH),'} "restaurante": { "cantidadOcupado":',(perOR),'"cantidadDisponible":',(100-perOR),'}'

        return Response(message, status=status.HTTP_200_OK)

    def post(self, request):
        """Parametros POST"""

        return Response(status=status.HTTP_200_OK)


class disponibilidad(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """CU19: Ver disponibilidad de habitaciones"""
        """parametros GET"""
        ocupacion_hotel = IngresosHotel.objects.aggregate(sumaO=Sum('habitaciones_ocupadas'))
        habit = Hotel.objects.aggregate(sumaH=Sum('habitaciones'))

        dispo: int = int(habit['sumaH']) - int(ocupacion_hotel['sumaO']) 
        message="Habitaciones:", habit["sumaH"],"Disponibles:",dispo

        return Response(message, status=status.HTTP_200_OK)

    def post(self, request):
        """Parametros POST"""

        return Response(status=status.HTTP_200_OK)


class monitorear_estado(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """CU20: Monitorear estado de las habitaciones"""
        """parametros GET"""


        return Response(HabitacionHotelSerializer(HabitacionHotel.objects.all(), many=True).data,status=status.HTTP_200_OK)

    def post(self, request):
        """parametros Post"""
        stat = HabitacionHotelSerializer(HabitacionHotel.objects.get(id_habitacion_hotel=request.data["id_habitacion_hotel"]))
        return Response(stat.data,status=status.HTTP_200_OK)


class ingreso_externo(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """CU21: Registrar Ingresos Externos"""
        """Parametros GET"""

        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        status_code: int
        """parametros POST"""
        required_fields = ["idRegistro","ingresos_externos"]
        message = "Ingreso registrado correctamente"
        not_provided = [required_field for required_field in required_fields if required_field not in request.data]
        if not_provided:
            message = "Faltan los campos: " + ", ".join(not_provided)
            status_code = status.HTTP_206_PARTIAL_CONTENT
        else:
            status_code = registrar_ingresos_externos(
                idRegistro=request.data['idRegistro'],
                ingresos_externos=request.data['ingresos_externos']
                )
            message = message if status_code == status.HTTP_200_OK else "error"
        return Response(message, status=status_code)

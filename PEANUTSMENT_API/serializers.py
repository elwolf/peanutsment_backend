from django.contrib.auth.models import User
from rest_framework import serializers

from PEANUTSMENT_API.models import IngresosHotel, Hotel, EstadoHabitacion, HabitacionHotel, IngresosParqueDiversiones, \
    IngresosRestaurante, Paquete, Restaurante, UsuarioMetodoPago, UsuarioPaquete


class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = ['id_hotel', 'habitaciones']


class EstadoHabitacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstadoHabitacion
        fields = ['id_estado_habitacion', 'descripcion']


class IngresosHotelSerializer(serializers.ModelSerializer):
    hotel = HotelSerializer()

    class Meta:
        model = IngresosHotel
        fields = ['id_ingresos_hotel', 'habitaciones_ocupadas', 'ingresos_externos', 'ingresos_sistema', 'fecha',
                  'hotel']


class HabitacionHotelSerializer(serializers.ModelSerializer):
    hotel = HotelSerializer()
    estado = EstadoHabitacionSerializer()

    class Meta:
        model = HabitacionHotel
        fields = ['id_habitacion_hotel', 'numero', 'estado', 'hotel', 'fecha_inicio_ocupacion', 'fecha_fin_ocupacion']


class PaqueteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paquete
        fields = ["id", "nombre", "precio", "costo_hotel", "costo_restaurante", "costo_parque", "description",
                  "url_imagen",
                  "fecha_inicio_vigencia", "fecha_fin_vigencia", "duracion"]


class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "first_name", "last_name", "email"]


class UsuarioMetodoPagoSerializer(serializers.ModelSerializer):
    usuario = UsuarioSerializer()

    class Meta:
        model = UsuarioMetodoPago
        fields = ["usuario", "numero_tarjeta", "cvv", "id", "nombreEnTarjeta", "vencimiento",
                  # "mesVencimiento", "anioVencimiento",
                  "numTelefono", "calleyNum", "estado", "ciudad", "colonia", "codigoPostal"]


class UsuarioPaqueteSerializer(serializers.ModelSerializer):
    usuario = UsuarioSerializer()
    usuario_metodo_pago = UsuarioMetodoPagoSerializer()
    paquete = PaqueteSerializer()

    class Meta:
        model = UsuarioPaquete
        fields = ["usuario", "usuario_metodo_pago", "paquete", "fecha_compra", "fecha_inicio", "fecha_fin"]


class IngresosParqueDiversionesSerializer(serializers.ModelSerializer):
    class Meta:
        model = IngresosParqueDiversiones
        fields = ['id_ingresos_parque_diversiones', 'ingresos_externos', 'ingresos_sistema', 'fecha']


class RestauranteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurante
        fields = ['id_restaurante', 'mesas']


class IngresosRestauranteSerializer(serializers.ModelSerializer):
    restaurante = RestauranteSerializer()

    class Meta:
        model = IngresosRestaurante
        fields = ['id_ingresos_restaurante', 'mesas_ocupadas', 'ingresos_sistema', 'ingresos_externos', 'fecha',
                  'restaurante']


class ChangePasswordSerializer(serializers.Serializer):
    model = User
    """
    Serializer for password change endpoint
    """

    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

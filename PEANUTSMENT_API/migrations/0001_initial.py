# Generated by Django 3.2.9 on 2021-12-11 02:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='EstadoHabitacion',
            fields=[
                ('id_estado_habitacion', models.IntegerField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Hotel',
            fields=[
                ('id_hotel', models.IntegerField(primary_key=True, serialize=False)),
                ('habitaciones', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='IngresosParqueDiversiones',
            fields=[
                ('id_ingresos_parque_diversiones', models.IntegerField(primary_key=True, serialize=False)),
                ('ingresos_externos', models.DecimalField(decimal_places=2, max_digits=16)),
                ('ingresos_sistema', models.DecimalField(decimal_places=2, max_digits=16)),
                ('fecha', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Paquete',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('precio', models.DecimalField(decimal_places=2, max_digits=18)),
                ('costo_hotel', models.DecimalField(decimal_places=2, max_digits=18, null=True)),
                ('costo_restaurante', models.DecimalField(decimal_places=2, max_digits=18, null=True)),
                ('costo_parque', models.DecimalField(decimal_places=2, max_digits=18, null=True)),
                ('description', models.CharField(max_length=2000)),
                ('url_imagen', models.CharField(max_length=2000)),
                ('fecha_inicio_vigencia', models.DateField()),
                ('fecha_fin_vigencia', models.DateField()),
                ('duracion', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Restaurante',
            fields=[
                ('id_restaurante', models.IntegerField(primary_key=True, serialize=False)),
                ('mesas', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='UsuarioMetodoPago',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero_tarjeta', models.CharField(max_length=17)),
                ('cvv', models.CharField(max_length=3)),
                ('nombreEnTarjeta', models.CharField(max_length=50)),
                ('mesVencimiento', models.DateField()),
                ('anioVencimiento', models.DateField()),
                ('numTelefono', models.CharField(max_length=10)),
                ('calleyNum', models.CharField(max_length=50)),
                ('estado', models.CharField(max_length=50)),
                ('ciudad', models.CharField(max_length=50)),
                ('colonia', models.CharField(max_length=50)),
                ('codigoPostal', models.CharField(max_length=5)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UsuarioPaquete',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_compra', models.DateTimeField()),
                ('fecha_inicio', models.DateTimeField()),
                ('fecha_fin', models.DateTimeField()),
                ('cancelado', models.BooleanField(default=False)),
                ('paquete', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='PEANUTSMENT_API.paquete')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('usuario_metodo_pago', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='PEANUTSMENT_API.usuariometodopago')),
            ],
        ),
        migrations.CreateModel(
            name='IngresosRestaurante',
            fields=[
                ('id_ingresos_restaurante', models.IntegerField(primary_key=True, serialize=False)),
                ('mesas_ocupadas', models.IntegerField()),
                ('ingresos_sistema', models.DecimalField(decimal_places=2, max_digits=16)),
                ('ingresos_externos', models.DecimalField(decimal_places=2, max_digits=16)),
                ('fecha', models.DateTimeField()),
                ('restaurante', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='PEANUTSMENT_API.restaurante')),
            ],
        ),
        migrations.CreateModel(
            name='IngresosHotel',
            fields=[
                ('id_ingresos_hotel', models.IntegerField(primary_key=True, serialize=False)),
                ('habitaciones_ocupadas', models.IntegerField()),
                ('ingresos_externos', models.DecimalField(decimal_places=2, max_digits=16)),
                ('ingresos_sistema', models.DecimalField(decimal_places=2, max_digits=16)),
                ('fecha', models.DateTimeField()),
                ('hotel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='PEANUTSMENT_API.hotel')),
            ],
        ),
        migrations.CreateModel(
            name='HabitacionHotel',
            fields=[
                ('id_habitacion_hotel', models.IntegerField(primary_key=True, serialize=False)),
                ('numero', models.IntegerField()),
                ('fecha_inicio_ocupacion', models.DateTimeField()),
                ('fecha_fin_ocupacion', models.DateTimeField()),
                ('estado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='PEANUTSMENT_API.estadohabitacion')),
                ('hotel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='PEANUTSMENT_API.hotel')),
            ],
        ),
    ]

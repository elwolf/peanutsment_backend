"""PEANUTSMENT URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from PEANUTSMENT_API.views import AgregarMetodoPago, DetallesCompra, EditarMetodoPago, EditarPerfil, \
    GestionarMetodoPago, HistorialCompras, HotelView, Login, Logout, VerPerfil, UserCRUD, ChangePasswordView, \
    CompraDePaquetesView, eliminarMetodo, verPaquetes, estadisticasFlujo, ocupacion, disponibilidad, \
    monitorear_estado, ingreso_externo

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/hotel/', HotelView.as_view()),
    path('api/login/', Login.as_view()),
    path('api/registro/', UserCRUD.as_view()),
    path('api/cambiar_contrasenia/', ChangePasswordView.as_view(), name='change-password'),
    path('api/recuperar_contrasenia/', include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('api/compra_paquete/', CompraDePaquetesView.as_view()),
    path('api/CancelarPaquete/<int:idCompra>/', CompraDePaquetesView.as_view()),
    path('api/ReagendarPaquete/', CompraDePaquetesView.as_view()),
    path('api/logout/', Logout.as_view()),
    # path('api/CancelarPaquete/<int:idCompra>/', Paquete.as_view()),
    path('api/HistorialCompras/<int:idCompra>/', HistorialCompras.as_view()),
    path("api/HistorialCompras/", DetallesCompra.as_view()),
    path("api/perfilUsuario/", VerPerfil.as_view()),
    path("api/EditarPerfil/", EditarPerfil.as_view()),
    path("api/GestionarMetodoPago/", GestionarMetodoPago.as_view()),
    path("api/AgregarMetodoPago/", AgregarMetodoPago.as_view()),
    path("api/EditarMetodoPago/", EditarMetodoPago.as_view()),
    path("api/eliminarMetodo/", eliminarMetodo.as_view()),
    path("api/verPaquetes/", verPaquetes.as_view()),
    path("api/estadisticasFlujo/", estadisticasFlujo.as_view()),
    path("api/ocupacion/", ocupacion.as_view()),
    path("api/disponibilidad/", disponibilidad.as_view()),
    path("api/monitorear_estado/", monitorear_estado.as_view()),
    path("api/ingreso_externo/", ingreso_externo.as_view()),
]

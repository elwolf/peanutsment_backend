django==3.2.9
djangorestframework==3.12.4
mysqlclient==2.1.0
django-rest-passwordreset==1.2.1
django-cors-headers
django-credit-cards